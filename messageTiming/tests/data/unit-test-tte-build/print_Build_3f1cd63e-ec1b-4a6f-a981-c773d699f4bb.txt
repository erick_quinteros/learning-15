[1] "memory after data read and cleanup"
            accountProduct                   accounts 
                    246536                     160496 
                      args                  BUILD_UID 
                       944                        136 
              CATALOGENTRY                catalogFile 
                       112                        232 
       changeModelSavePath    checkNumOfFilesInFolder 
                     10248                        952 
                   cleanUp                        con 
                     17016                        680 
                     con_l                     config 
                       680                       4600 
                 configUID           copyMockBuildDir 
                       120                     151664 
                  customer            DAYS_AFTER_SEND 
                       120                         56 
          DAYS_AFTER_VISIT               DAYS_IN_WEEK 
                        56                         56 
              DAYS_PREDICT                     dbhost 
                        56                        120 
                    dbname                 dbpassword 
                       120                        112 
                    dbuser               DRIVERMODULE 
                       112                        136 
        expect_file_exists     expect_file_not_exists 
                      3496                       3680 
        expect_num_of_file       expect_str_end_match 
                      5472                       1736 
          expect_str_match    expect_str_pattern_find 
                     15552                       5472 
expect_str_pattern_notfind     expect_str_start_match 
                      9072                       2520 
                      func            getDBConnection 
                       136                      38008 
         getDBConnectionCS    getDBConnectionLearning 
                      4496                       3032 
      getDBConnectionStage               getTTEparams 
                      3032                     172352 
                   homedir                          i 
                       168                         56 
              interactions               LOOKBACK_MAX 
                   1629488                         56 
        MAX_RECOMMEND_DAYS              messageTiming 
                        56                     154368 
         messageTimingData                    METHODS 
                    297992                        248 
                  NA_VALUE                    plotDir 
                        56                        264 
                   plotRef                       port 
                       232                         56 
             processConfig    ProductInteractionTypes 
                      6096                        184 
                  products                 readConfig 
                      2000                       6352 
               readLogFile            readLogFileBase 
                      2488                       2352 
              readMockData           readModuleConfig 
                      3568                       5568 
          readScoreLogFile              resetMockData 
                      2488                     244072 
                   RUN_UID                     runDir 
                       152                        232 
                  runStamp          setDBDataEncoding 
                       136                       4576 
         setupMockBuildDir                 setupTable 
                      9512                      33560 
                startTimer                    testdir 
                       344                        136 
                 timeStamp                  tteParams 
                       152                       4144 
          ttePredictReport                VAL_NO_DATA 
                     67248                         56 
                versionUID                   WORKBOOK 
                       152                        688 
         writeMockDataToDB 
                     41472 
[1] "accountId"                "channelPreferenceNeg_akt"
[3] "channelPreferencePos_akt" "sampleAO_akt"            
Classes ‘data.table’ and 'data.frame':	560 obs. of  4 variables:
 $ accountId               : int  1616 1616 1653 1653 1814 1814 2137 2137 2541 2541 ...
 $ channelPreferenceNeg_akt: chr  "" "" "" "" ...
 $ channelPreferencePos_akt: chr  "" "" "" "" ...
 $ sampleAO_akt            : num  NA NA NA 0 NA 0 NA NA NA 0 ...
 - attr(*, "sorted")= chr "accountId"
 - attr(*, ".internal.selfref")=<externalptr> 
          V1
1:   integer
2: character
3: character
4:   numeric
[1] "channelPreferenceNeg_akt" "channelPreferencePos_akt"
[1] "A"
  |                                                                              |                                                                      |   0%  |                                                                              |======================================================================| 100%
  |                                                                              |                                                                      |   0%  |                                                                              |==                                                                    |   3%  |                                                                              |====                                                                  |   6%  |                                                                              |=====                                                                 |   7%  |                                                                              |============                                                          |  17%  |                                                                              |========================                                              |  35%  |                                                                              |=============================                                         |  42%  |                                                                              |================================                                      |  46%  |                                                                              |===================================                                   |  50%  |                                                                              |=====================================                                 |  53%  |                                                                              |===============================================                       |  67%  |                                                                              |=========================================================             |  81%  |                                                                              |======================================================================| 100%
[1] "Message: TARGET"
Model Details:
==============

H2ORegressionModel: drf
Model ID:  DRF_model_R_1572025652887_1 
Model Summary: 
  number_of_trees number_of_internal_trees model_size_in_bytes min_depth
1             100                      100              495835        10
  max_depth mean_depth min_leaves max_leaves mean_leaves
1        10   10.00000        243        502   390.48000


H2ORegressionMetrics: drf
** Reported on training data. **
** Metrics reported on Out-Of-Bag training samples **

MSE:  0.00689084
RMSE:  0.08301109
MAE:  0.02284565
RMSLE:  0.05902129
Mean Residual Deviance :  0.00689084



H2ORegressionMetrics: drf
** Reported on cross-validation data. **
** 2-fold cross-validation on training data (Metrics computed for combined holdout predictions) **

MSE:  0.007476663
RMSE:  0.0864677
MAE:  0.0237
RMSLE:  0.06164055
Mean Residual Deviance :  0.007476663


Cross-Validation Metrics Summary: 
                               mean           sd  cv_1_valid   cv_2_valid
mae                     0.023700114 9.1443035E-5 0.023829434  0.023570795
mean_residual_deviance 0.0074770795 3.3375007E-4 0.007949073 0.0070050857
mse                    0.0074770795 3.3375007E-4 0.007949073 0.0070050857
r2                         0.529991 0.0106566595   0.5149202    0.5450617
residual_deviance      0.0074770795 3.3375007E-4 0.007949073 0.0070050857
rmse                     0.08642698 0.0019308211 0.089157574   0.08369639
rmsle                    0.06161801 0.0012177083  0.06334011  0.059895914
[1] "B"
  |                                                                              |                                                                      |   0%  |                                                                              |==================================================================    |  94%  |                                                                              |======================================================================| 100%
  |                                                                              |                                                                      |   0%  |                                                                              |======                                                                |   8%  |                                                                              |=========                                                             |  13%  |                                                                              |============                                                          |  17%  |                                                                              |==============                                                        |  20%  |                                                                              |=========================                                             |  36%  |                                                                              |==============================                                        |  42%  |                                                                              |=================================                                     |  47%  |                                                                              |===================================                                   |  50%  |                                                                              |=======================================                               |  55%  |                                                                              |===============================================                       |  67%  |                                                                              |============================================================          |  86%  |                                                                              |======================================================================| 100%
[1] "Message: TARGET"
Model Details:
==============

H2ORegressionModel: drf
Model ID:  DRF_model_R_1572025652887_2 
Model Summary: 
  number_of_trees number_of_internal_trees model_size_in_bytes min_depth
1             100                      100              481705        10
  max_depth mean_depth min_leaves max_leaves mean_leaves
1        10   10.00000        246        490   379.10000


H2ORegressionMetrics: drf
** Reported on training data. **
** Metrics reported on Out-Of-Bag training samples **

MSE:  0.007362487
RMSE:  0.08580494
MAE:  0.02410923
RMSLE:  0.0609752
Mean Residual Deviance :  0.007362487



H2ORegressionMetrics: drf
** Reported on cross-validation data. **
** 2-fold cross-validation on training data (Metrics computed for combined holdout predictions) **

MSE:  0.007958355
RMSE:  0.08920961
MAE:  0.02499172
RMSLE:  0.06347819
Mean Residual Deviance :  0.007958355


Cross-Validation Metrics Summary: 
                              mean           sd   cv_1_valid  cv_2_valid
mae                     0.02499184 1.7366008E-4  0.025237434 0.024746248
mean_residual_deviance 0.007958543 2.6315992E-4 0.0083307065 0.007586378
mse                    0.007958543 2.6315992E-4 0.0083307065 0.007586378
r2                       0.4995026  0.007847462    0.4884046  0.51060057
residual_deviance      0.007958543 2.6315992E-4 0.0083307065 0.007586378
rmse                    0.08918626 0.0014753388  0.091272704  0.08709981
rmsle                    0.0634667  8.770021E-4   0.06470697  0.06222643
[1] "C"
  |                                                                              |                                                                      |   0%  |                                                                              |===============                                                       |  21%  |                                                                              |======================================================================| 100%
  |                                                                              |                                                                      |   0%  |                                                                              |                                                                      |   1%  |                                                                              |============                                                          |  18%  |                                                                              |===================                                                   |  27%  |                                                                              |=======================                                               |  33%  |                                                                              |==============================                                        |  42%  |                                                                              |==================================================                    |  72%  |                                                                              |======================================================================| 100%
[1] "Message: TARGET"
Model Details:
==============

H2ORegressionModel: drf
Model ID:  DRF_model_R_1572025652887_3 
Model Summary: 
  number_of_trees number_of_internal_trees model_size_in_bytes min_depth
1             100                      100               31153         1
  max_depth mean_depth min_leaves max_leaves mean_leaves
1        10    7.24000          2         47    20.16000


H2ORegressionMetrics: drf
** Reported on training data. **
** Metrics reported on Out-Of-Bag training samples **

MSE:  0.01518167
RMSE:  0.1232139
MAE:  0.04462151
RMSLE:  0.08988278
Mean Residual Deviance :  0.01518167



H2ORegressionMetrics: drf
** Reported on cross-validation data. **
** 2-fold cross-validation on training data (Metrics computed for combined holdout predictions) **

MSE:  0.0152667
RMSE:  0.1235585
MAE:  0.04485117
RMSLE:  0.09016942
Mean Residual Deviance :  0.0152667


Cross-Validation Metrics Summary: 
                              mean           sd  cv_1_valid  cv_2_valid
mae                    0.044851188  1.745675E-5   0.0448265 0.044875875
mean_residual_deviance 0.015266959 2.3231097E-4 0.014938422 0.015595496
mse                    0.015266959 2.3231097E-4 0.014938422 0.015595496
r2                      0.03941256 0.0015557605  0.04161274 0.037212387
residual_deviance      0.015266959 2.3231097E-4 0.014938422 0.015595496
rmse                    0.12355238 9.4013143E-4  0.12222283  0.12488193
rmsle                   0.09016632 5.8094313E-4  0.08934474   0.0909879
