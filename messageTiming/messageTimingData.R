#
#
# aktana-learning model building code for Aktana Learning Engines.
#
# description: This is initiallization code
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#
##########################################################

messageTimingData <- function(con, dbname_cs, dbname_learning, tteParams)
{
    library(data.table)
    library(RMySQL)
    library(futile.logger)
    
    flog.info("Entered loadMessageTimingData")
    dictraw <- readV3dbFilter(con, productUID="All", prods=tteParams[["prods"]]); 
    
    for(i in 1:length(dictraw))assign(names(dictraw)[i],dictraw[[i]]) # assign variable name with corresponding data

    rm(dictraw)
    gc(); gc(); gc()

    EventTypes <- tteParams[["EventTypes"]]
    predictRunDate <- tteParams[["predictRunDate"]]
    LOOKBACK <- tteParams[["LOOKBACK"]]

    events <- events[eventTypeName %in% EventTypes, list(accountId,eventDate,eventTypeName,physicalMessageUID,productName)]
    setnames(events,c("eventTypeName","eventDate"),c("type","date"))
    events[,date:=as.Date(date)]

    if (tteParams[["useDeliveredEmail"]]) {
        # filter data based on "Sent_Email_vod__c_arc" to get delivered emails only for design matrix
        # get delivered status information from DB

        deliveredInt <- dbGetQuery(con, sprintf("SELECT Id FROM %s.Sent_Email_vod__c_arc WHERE Status_vod__c = 'Delivered_vod';", dbname_learning))
        deliveredInt <- deliveredInt[1:dim(deliveredInt)[1],1] # convert dataframe to vector
        
        # filter the message delivered
        flog.info('filtering interaction data based on delivered_or_not information from Sent_Email_vod__c_arc')
        interactions <- interactions[externalId %in% deliveredInt | interactionTypeName == 'VISIT' | interactionTypeName == 'WEB_INTERACTION']
        interactions$externalId <- NULL
    }
    
    # filter out letters from interactions
    call2sample <- data.table(dbGetQuery(con, sprintf("select Id from %s.Call2_Sample_vod__c", dbname_cs)))
    
    # filter interactions data using LOOKBACK
    interactions <- interactions[date >= (predictRunDate - LOOKBACK)]

    interactions <- interactions[!externalId %in% call2sample$Id]
    
    interactions <- interactions[, list(accountId,physicalMessageUID,productInteractionTypeName,date,productName)]
    setnames(interactions,c("productInteractionTypeName"),c("type"))
    interactions <- rbind(interactions,events,fill=T)

    flog.info("Merge accounts and accountProduct tables")
###### merge accounts into accountproduct table
    accounts[,c("externalId","createdAt","updatedAt","facilityId","accountName","isDeleted"):=NULL]
    accountProduct[,c("createdAt","updatedAt"):=NULL]
    
    names(accounts) <- gsub("-","_",names(accounts))  # remove -'s from names
    names(accountProduct) <- gsub("-","_",names(accountProduct))  # remove -'s from names

    colClass <- sapply(accounts, class)
    charColsAccounts <- names(colClass[colClass=="character"])
    accounts <- accounts[,(charColsAccounts):=lapply(.SD, function(x) {return(gsub('[\n|\r]','',x))}),.SDcols=charColsAccounts]  # spark dataframe cannot have \n in column name

    colClass <- sapply(accountProduct, class)
    charColsAccountProduct <- names(colClass[colClass=="character"])
    accountProduct <- accountProduct[,(charColsAccountProduct):=lapply(.SD, function(x) {return(gsub('\n','',x))}),.SDcols=charColsAccountProduct]  # spark dataframe cannot have \n in column name

    # find out charCols for later use before convert to spark
    charCols <- c(charColsAccounts,charColsAccountProduct)
    charCols <- charCols[charCols!="productName"]

    tA <- data.table()
    for(i in 1:dim(products)[1])
    {
        accounts$productName <- products[i]$productName
        tA <- rbind(tA,accounts)
    }

    accountProduct <- merge(accountProduct,tA,by=c("accountId","productName"),all.x=TRUE,allow.cartesian=TRUE)

    flog.info("Remove bad characters")
####### remove bad characters from the accountProduct metrics

    replaceFunc <- function(x) {return(gsub('[[:blank:]|,|>|<]','_',x))}
    accountProduct <- cbind(accountProduct[,-..charCols],accountProduct[,..charCols][,lapply(.SD, replaceFunc)])

    rm(call2sample)
    rm(events)
    rm(tA)
    gc(); gc(); gc()

    flog.info("Return from loadMessageTimingData")
    return(list(interactions = interactions, accountProduct = accountProduct, products = products, accounts = accounts))
}
