##########################################################
#
#
# aktana- messageSequence Rebuild.
#
# description: Rebuild models
#
# created by : wendong.zhu@aktana.com
#
# created on : 2018-02-01
#
# Copyright AKTANA (c) 2018.
#
#
####################################################################################################################
library(RMySQL)
library(futile.logger)
library(properties)

#################################################
## function: get connection handle of learning DB
#################################################
getDBConnectionLearning <- function()
{
  dbnameLearning <<- dbname_learning
  drv <- dbDriver("MySQL")

  if(exists("port")){
      tryCatch(con_l <- dbConnect(drv,user=dbuser,password=dbpassword,host=dbhost,dbname=dbnameLearning,port=port),
               error = function(e) {
                 flog.error('Error in connecting to db: %s', dbnameLearning, name='error')
                 quit(save = "no", status = 64, runLast = FALSE) # user-defined error code 64 for failure of connecting db
               })
  } else {
      tryCatch(con_l <- dbConnect(drv,user=dbuser,password=dbpassword,host=dbhost,dbname=dbnameLearning),
               error = function(e) {
                 flog.error('Error in connecting to db: %s', dbnameLearning, name='error')
                 quit(save = "no", status = 64, runLast = FALSE) # user-defined error code 64 for failure of connecting db
               })
  }

  return(con_l)
}

################################################
## function: get connection handle of DSE DB
################################################
getDBConnection <- function()
{
  drv <- dbDriver("MySQL")

  if(exists("port")){
      tryCatch(con <- dbConnect(drv,user=dbuser,password=dbpassword,host=dbhost,dbname=dbname,port=port),
               error = function(e) {
                 flog.error('Error in connecting to db: %s', dbname, name='error')
                 quit(save = "no", status = 64, runLast = FALSE) # user-defined error code 64 for failure of connecting db
               })
  } else {
      tryCatch(con <- dbConnect(drv,user=dbuser,password=dbpassword,host=dbhost,dbname=dbname),
               error = function(e) {
                 flog.error('Error in connecting to db: %s', dbname, name='error')
                 quit(save = "no", status = 64, runLast = FALSE) # user-defined error code 64 for failure of connecting db
               })
  }

  return(con)
}

################################################
## function: get build stats from learningBuild
################################################
getBuildExecutionStatus<- function(buildUID, connection_learning, logStatus=FALSE)
{
  if (logStatus) {
    result <- dbGetQuery(connection_learning, sprintf("SELECT * FROM LearningBuild WHERE learningBuildUID='%s';", buildUID))
    flog.info("buildUID:%s (configUID=%s, versionUID=%s) '%s'", result$learningBuildUID, result$learningConfigUID, result$learningVersionUID, result$executionStatus)
  } else {
    result <- dbGetQuery(connection_learning, sprintf("SELECT executionStatus FROM LearningBuild WHERE learningBuildUID='%s';", buildUID))
  }
  return(result$executionStatus)
}

#################################################################################################
## Main Program
## Description:
##   RebuildModel.r will first find all externalIds (or CONFIG_UIDs) from table MessageAlgorithm;
##   then in nightly directory which contains deployed configs, get the buildUID from each config;
##   and rebuild those MSO models.
##   Rebuild results such as models, workbook etc. are saved in builds/$BUILD_UID/.
##   Some results are saved in LearningDB.
#################################################################################################

DRIVERMODULE <- "rebuildModel.r"

# receive parameters command line
args <- commandArgs(T)

if(length(args)==0){
    print("No arguments supplied.")
    if(batch)quit(save = "no", status = 1, runLast = FALSE)
} else {
        print("Arguments supplied.")
        for(i in 1:length(args)){
          eval(parse(text=args[[i]]))
          print(args[[i]]);
    }
}

# establish db connection to learning DB (con_l) and DSE DB (con)
connection_dse <- getDBConnection()
connection_learning <- getDBConnectionLearning()

# read config data from MessageAlgorithm
modelsToBuild <- dbGetQuery(connection_dse,"Select messageAlgorithmId, externalId from MessageAlgorithm where isDeleted=0 and isActive=1 and messageAlgorithmType=1;")

# if modelsToBuild is empty, give warning and exit
if (nrow(modelsToBuild) == 0) {
  flog.warn("No model to build!")
  # Disconnet DB and release handles
  dbDisconnect(connection_dse)
  dbDisconnect(connection_learning)
  quit(save = "no", status = 1, runLast = FALSE) # exit
}

# get list of configUID that need to be rebuilt
configUIDs <- unique(modelsToBuild$externalId) # unique configs

# get list of deployed versinUID that need to be rebuilt
getDeployedVersion <- function (configuid) {
  propertiesFilePath <- sprintf("%s/builds/nightly/%s/learning.properties",homedir,configuid);
  versionUID <- read.properties(propertiesFilePath)[["versionUID"]]
}
versionUIDs <- lapply(configUIDs, getDeployedVersion) # versionUIDs is list, which its elements could be accessed using versionUIDs[[1]]

# source API call related scripts
source(sprintf("%s/common/APICall/global.R",homedir))
# initialize API call params
init(TRUE, paste(homedir,"/common/APICall",sep=""))
# Authenticate and get a token
Authenticate(glblLearningAPIurl)


MAX_BUILD_RETRY_COUNT <- 12
SUBMIT_SUCCESS_CODE <- 200
SUBMIT_FAILURE_CODE <- 409
WAIT_TIME_BETWEEN_BUILDS <- 600# In seconds = 40 mins

# TO-TEST::
#configUID <- '52e920c7-edac-4677-bac4-fbd18431a9b4'
#configUID <- '08e04b52-c321-4dd3-8938-d7152b6976b5'
#configUID <- 'AKT_REM_V0'
#configUIDs <- list('52e920c7-edac-4677-bac4-fbd18431a9b4','08e04b52-c321-4dd3-8938-d7152b6976b5', '41380f52-b0df-4437-a9a0-7be5e2113a5d')
#configUIDs
#versionUIDs <- list('302435ea-ef22-4c04-993a-bed93ff2a772', '41380f52-b0df-4437-a9c0-7be5e2113a5d')
#index <- 1

# List to store list of all submitted BuildIds
submittedReBuildUIDs <- list()


for(index in 1:length(configUIDs))
{
  #print(index)
  #print(configUIDs[index])

  # Get configUID and versionUID for calling an API
  configUID <- configUIDs[[index]]
  versionUID <- versionUIDs[[index]]

  buildRetryCount <- 0
  isBuildSubmitSuccess <- FALSE

  # Call learning here to do build one by one
  while(isBuildSubmitSuccess == FALSE & buildRetryCount < MAX_BUILD_RETRY_COUNT)
  {
    flog.info("Building Config - %s Build Retry Count - %d", configUID, buildRetryCount)
    result <- putAPIJsonRaw(glblLearningAPIurl, paste("LearningConfig/",configUID,"/versions/",versionUID,"/build?deploy=true",sep = ""))

    if (httr::status_code(result) == SUBMIT_SUCCESS_CODE)
    {
      # The rebuild API call is successful
      isBuildSubmitSuccess <- TRUE
      submittedBuildUID <- httr::content(result)$learningBuildUID
      submittedReBuildUIDs <- c(submittedReBuildUIDs, submittedBuildUID)
      flog.info("Submitted rebuild job for ConfigUID: %s with BuildUID: %s", configUID, submittedBuildUID)
      Sys.sleep(WAIT_TIME_BETWEEN_BUILDS)
    }
    else if(httr::status_code(result) == SUBMIT_FAILURE_CODE)
    {
      # The rebuild API call is failed because of-
      #   1. Another build process is running
      #   2. There is some other issue in PUT API call
      isBuildSubmitSuccess <- FALSE
      buildRetryCount <- buildRetryCount + 1
      Sys.sleep(WAIT_TIME_BETWEEN_BUILDS)
    }
    else
    {
      # Unknown response from the API
      flog.info("Unknown resposnse from PUT API call for Config Build Response-- \nCONTENT:%s \nCODE:%d", httr::content(result), httr::status_code(result))
      break

    }
  }

  if(isBuildSubmitSuccess == FALSE & buildRetryCount == MAX_BUILD_RETRY_COUNT)
  {
    # The current configUID is build attempted for rebuild for three times in 40 mins interval still not able to submit it successfully
    flog.warn("Unable to submit rebuild job for ConfigUID: %s", configUID)
  }
}

# check the status of the submitted builds
numOfBuilds <- length(submittedReBuildUIDs)
if (numOfBuilds>0) { # has builds submitted successfully
  
  # Checking the status of the last submitted build every 10mins, and sleep if the status is still running to make sure the last build finish running
  lastSubmittedBuildUID <- submittedReBuildUIDs[[numOfBuilds]]
  checkBuildStatusCount <- 0
  buildStatus <- getBuildExecutionStatus(lastSubmittedBuildUID, connection_learning)
  while(buildStatus == "running" & checkBuildStatusCount < MAX_BUILD_RETRY_COUNT) {
    Sys.sleep(WAIT_TIME_BETWEEN_BUILDS)
    buildStatus <- getBuildExecutionStatus(lastSubmittedBuildUID, connection_learning)
    checkBuildStatusCount <- checkBuildStatusCount + 1
  }
  flog.info("Finished the last build in MSO rebuild:%s (rebuild for configUID=%s versionUID=%s), with executionStatus='%s'", lastSubmittedBuildUID, configUID, versionUID, buildStatus)
  
  # check the status of all the submitted builds, throw error if any of it fail
  allBuildStatus <- sapply(submittedReBuildUIDs, getBuildExecutionStatus, connection_learning=connection_learning, logStatus=TRUE)
  numOfFailedBuilds <- sum(allBuildStatus != "success")
  
  # check if every config is submitted and all submitted build is success, otherwise throw error
  if (length(configUIDs) > numOfBuilds) { # has failure in submtting build
    stop(sprintf("Rebuild Failed: %d conifgs not submitted for build, and %d submitted build fail",length(configUIDs)-numOfBuilds,numOfFailedBuilds))
  } else if (length(configUIDs) == numOfBuilds) { # no failure in submtting build
    if (numOfFailedBuilds!=0) {
      stop(sprintf("Rebuild Failed: all conifgs submitted for build, but %d submitted build fail",numOfFailedBuilds))
    }
  } else {
    stop("Rebuild Failed: duplicated builds submitted for conifgs")
  }
  
} else { # no build submitted successfully
  stop("Rebuild Failed: No build submitted successfully")
}



# Disconnet DB and release handles
dbDisconnect(connection_dse)
dbDisconnect(connection_learning)
