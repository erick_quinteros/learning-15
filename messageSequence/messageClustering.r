##########################################################
#
#
# aktana- messageSequence estimates estimates Aktana Learning Engines.
#
# description: driver pgm
#
# created by : marc.cohen@aktana.com
#
# created on : 2016-11-01
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################
library(data.table)
library(text2vec)
library(fpc)
library(RMySQL)
library(futile.logger)
library(reticulate)
library(dplyr)
library(jiebaR)
library(foreach)
library(doMC)
library(parallel)
library(RMeCab)
library(stringi)
library(stringr)

THRESHOLD_TO_CLUSTER <- 3  # row dimension of distance matrix for clustering
TOP_N <- 3  # top N keywords

numberCores <- detectCores() - 4
numberCores <- ifelse(numberCores <= 0, 1, numberCores) # set default 

################################################ 
# function: get message topic labels in Chinese
# Input: 
#        messageTopic
#        products
# Return:
#        results
################################################ 
get_message_topic_labels_cn <- function(messageTopic, products)
{
  registerDoMC(numberCores)
  
  setnames(products, "externalId", "productUID")
  
  #messageTopic <- merge(messageTopic, products, by="productUID")
  
  # group by productUID & emailTopicId and concatenate text of each message
  # then retrieve keywords of each cluster text
  messageTopicCluster <- messageTopic[, lapply(.(text), paste0, collapse=" "), by = c("productUID", "emailTopicId")]
  setnames(messageTopicCluster, "V1", "text")
  
  starter <- Sys.time()

  # multi-core parallel processing

  # initialize worker using topn keywords
  keys <- worker("keywords", topn = TOP_N)
      
  # function to get email topic name    
  getEmailTopicName <- function(text) {
      # retrieve keywords
      words <- keywords(text, keys)
      return(paste0(words, collapse = " "))
  }

  messageTopicCluster[, emailTopicName := mcmapply(getEmailTopicName, text, mc.cores=numberCores)]
  messageTopicCluster$text <- NULL
  
  # merge cluster containing keywords into messageTopic
  messageTopic <- merge(messageTopic, messageTopicCluster, by = c("productUID", "emailTopicId"))
  messageTopic <- messageTopic[, .(messageUID, name, documentDescription, emailSubject, productUID, emailTopicId, emailTopicName)]

  flog.info("parallel processing time is %s", Sys.time()-starter)

  return(messageTopic)
}


get_message_topic_labels_jp <- function(messageTopic, products)
{
  registerDoMC(numberCores)
  
  setnames(products, "externalId", "productUID")
  
  # messageTopic <- merge(messageTopic, products, by="productUID")
  
  # group by productUID & emailTopicId and concatenate text of each message
  # then retrieve keywords of each cluster text
  messageTopicCluster <- messageTopic[, lapply(.(text), paste0, collapse=" "), by = c("productUID", "emailTopicId")]
  setnames(messageTopicCluster, "V1", "text")
  
  # starter <- Sys.time()

  # multi-core parallel processing

  # initialize worker using topn keywords
  # keys <- worker("keywords", topn = TOP_N)
      
  # function to get email topic name    
  getEmailTopicName <- function(text) {
      jp_tokenizer <- create_japanese_tokenizer(use_filter=TRUE)
      all_words <- jp_tokenizer(text)
      words_freq <- freq(all_words)
      indx <- sort(words_freq$freq, decreasing = TRUE, index.return=TRUE)$ix[1:TOP_N]
      words <- words_freq$char[indx]
      return(paste0(words, collapse = " "))
  }

  messageTopicCluster[, emailTopicName := mcmapply(getEmailTopicName, text, mc.cores=numberCores)]
  # messageTopicCluster <- merge(messageTopicCluster, products, by="productUID")
  # messageTopicCluster[, emailTopicName := paste0(productName, emailTopicName, sep=' ')]
  messageTopicCluster$text <- NULL
  # messageTopicCluster$productName <- NULL
  
  # merge cluster containing keywords into messageTopic
  messageTopic <- merge(messageTopic, messageTopicCluster, by = c("productUID", "emailTopicId"))
  messageTopic <- messageTopic[, .(messageUID, name, documentDescription, emailSubject, productUID, emailTopicId, emailTopicName)]

  # flog.info("parallel processing time is %s", Sys.time()-starter)

  return(messageTopic)
}


create_japanese_tokenizer <- function(use_filter = FALSE) {
  # Use RMecab
  raw_tokenizer <- function(s) {
    rmecab_result <- RMeCabC(as.character(s))
    return (data.frame(
      "term" = as.vector(unlist(rmecab_result)),
      "part" = sapply(rmecab_result, names)
    ))
  }
  
  if (use_filter) {
    is_acceptable_japanese_term <- function(term) {
      # Filter out length-1 tokens, unless it is a Kanji or all-hiragana
      is_number_only <- str_detect(string = term, pattern = "^[0-9]*$")
      is_hiragana_only <- str_detect(string = term, pattern = "\\p{Hiragana}")
      is_short_non_kanji <- stri_length(term) == 1 && !str_detect(string = term, pattern = "\\p{Han}")
      return (!is_number_only && !is_hiragana_only && !is_short_non_kanji)
    }
    
    tokenizer <- function(s) {
      tokens <- raw_tokenizer(s)
      return (tokens
              # Get only noun and verbs
              %>% filter(part %in% c("名詞","動詞") & sapply(tokens[['term']], is_acceptable_japanese_term))
              %>% distinct()
      )
    }
  } else {
    tokenizer <- raw_tokenizer
  }
  
  # Returns the result as a list
  return (function(s) {
    tokens <- tokenizer(s)
    return (as.character(tokens[['term']]))
  })
}


########################################
# function - messageClustering
########################################
messageClustering <- function (con_cs, con_l, con) {
  
  flog.info("start running messageClustering")
  
  # source python script
  use_python("/usr/bin/python3")
  source_python(sprintf("%s/messageSequence/message_cluster_labeler.py",homedir))
  
  dbGetQuery(con_cs,"SET NAMES utf8")
  
  # load data
  recordType <-  data.table(dbGetQuery(con_cs,"SELECT * FROM RecordType;"))
  approvedDoc <- data.table(dbGetQuery(con_cs,sprintf("SELECT * FROM Approved_Document_vod__c WHERE IsDeleted=0 and RecordTypeId='%s';",recordType[Name=="Email_Template_vod" & SobjectType=="Approved_Document_vod__c"]$Id)))
  
  cols <- names(approvedDoc)
  cols <- gsub("_vod__c","",cols)
  cols <- gsub("__c","",cols)
  names(approvedDoc) <- cols
  
  approveD <- approvedDoc[,c("Id","Name","Document_Description","Email_Subject","Status","Product","Email_HTML_1"),with=F]
  
  prods <- unique(approveD$Product)
  AP <- approveD
  
  # do the analysis on the concatenation of Name, the document description and the email subject
  AP[,messageConcat:=paste(Name, Document_Description,Email_Subject,sep=" ")]
  AP$Email_HTML_1 <- NULL
  fwrite(AP, sprintf("%s/builds/%s/APinClustering.csv", homedir, BUILD_UID), nThread = getDTthreads())
  
  # cleanup text to be used for the analysis
  AP$messageConcat <- tolower(AP$messageConcat)
  AP$messageConcat <- gsub(":","",AP$messageConcat)
  AP$messageConcat <- gsub("-","",AP$messageConcat)
  AP$messageConcat <- gsub("_","",AP$messageConcat)
  AP$messageConcat <- gsub("&"," and ",AP$messageConcat)
  AP$messageConcat <- gsub("^ *|(?<= ) | *$", "", AP$messageConcat, perl = TRUE)
  AP$messageConcat <- gsub("\n","",AP$messageConcat)
  AP$messageConcat <- gsub(".com","",AP$messageConcat)
  AP$messageConcat <- gsub("nbsp;","",AP$messageConcat)
  
  # use customer name convention to determin if the customer is of Chinese language
  isChinese <- endsWith(toupper(customer), "CN")
  isJapanese <- endsWith(toupper(customer), "JP")

  if (isChinese) {
      user.dict <- sprintf("%s/messageSequence/user_dict.txt", homedir)
      jieba<-jiebaR::worker(user=user.dict)

      tok_fun_cn <-function(strings) 
          plyr::llply(strings, segment, jieba)

      data <- fread(sprintf("%s/messageSequence/stop_words_file.csv", homedir), header = F)
      stop_words <- data$V1
  }

  if (isJapanese) {
      jp_tokenizer <- create_japanese_tokenizer()
      tok_fun_jp <-function(strings) 
          plyr::llply(strings, jp_tokenizer)

      # data <- fread(sprintf("%s/messageSequence/stop_words_file.csv", homedir), header = F)
      # stop_words <- data$V1
  }


  # analyze each product message collection separately
  startTimer <- Sys.time()
  messageTopic <- data.table()
  for(i in prods)                                                              # separate analysis for each product
  {
    print(i)
    ad <- AP[Product==i]
    
    if (isChinese) {
        train <- itoken(ad$messageConcat,preprocessor=identity, tokenizer=tok_fun_cn, ids=ad$Id, progressbar=F) # tokenize Chinese language
        vocab <- create_vocabulary(train, ngram=c(1L,2L), stopwords = stop_words)  
    } else if (isJapanese){
        train <- itoken(ad$messageConcat,preprocessor=identity,tokenizer=tok_fun_jp,ids=ad$Id,progressbar=F)  # pull out the tokens from the text to be analyzed
        vocab <- create_vocabulary(train, ngram=c(1L,2L))     # create a vocab of one-word and 2-word ngrams

    } else {
        train <- itoken(ad$messageConcat,preprocessor=tolower,tokenizer=word_tokenizer,ids=ad$Id,progressbar=F)  # pull out the tokens from the text to be analyzed
        vocab <- create_vocabulary(train, ngram=c(1L,2L))     # create a vocab of one-word and 2-word ngrams
    }

    vectorizer <- vocab_vectorizer(vocab)                                                           # vectorize the vocabulary
    vocab <- prune_vocabulary(vocab,term_count_max=50L,doc_proportion_max=.50)                      # prune the vocabulary removing words that appear often and in many messages

    if(nrow(vocab) == 0) next                                                                       # skip empty vocab
    
    dtm <- create_dtm(train,vectorizer)                                                             # create a document term matrix
    mma <- dist2(dtm,method="cosine")                                                               # find the distance between each document based on the dtm
    
    if(dim(mma)[1] <= THRESHOLD_TO_CLUSTER) next                                                    # skip if there are few words

    print("start clustering ...")
    hc <- pamk(mma,krange=1:(dim(mma)[1]-1))$pamobject$clustering                                   # use partitioning around medoids to create clustering
    print("end clustering.")
    t <- data.table(Id=names(hc),cluster=hc)
    
    ap <- ad[,c("Id","Name","Document_Description","Email_Subject"),with=F]
    ap <- merge(ap,t,by="Id")                                                                       # save the results
    names(ap) <- c("messageUID","name", "documentDescription","emailSubject","emailTopicId")
    ap$productUID <- i
    messageTopic <- rbind(ap,messageTopic,fill=T)                                                   # messageTopic table contains results for all products
  }
  # done with analysis
  flog.info("get messageTopic Time = %s", Sys.time()-startTimer)
  
  # fill in for non-scored messages
  AP[,c("messageConcat","Status"):= NULL]
  
  if (isChinese) {
    # processing non-clustered Chinese messages
    names(AP) <- c("messageUID","name", "documentDescription","emailSubject","productUID")
    messagesAssigned <- messageTopic$messageUID
    
    # get the messages not being clustered
    t <- AP[!messageUID %in% messagesAssigned]  
    
    # create a sequence starting from maxId+1
    s <- seq(from = max(messageTopic$emailTopicId)+1, length.out = nrow(t)) 
    
    # assign the sequence number as new cluster id
    t$emailTopicId <- s
    
    # combine
    messageTopic <- rbind(messageTopic,t)
    
  } else {  
    # throw all non-clustered messages into one group
    AP$emailTopicId <- max(messageTopic$emailTopicId)+1                                               
    names(AP) <- c("messageUID","name","documentDescription","emailSubject","productUID","emailTopicId")
    messagesAssigned <- messageTopic$messageUID
    t <- AP[!messageUID %in% messagesAssigned]
    messageTopic <- rbind(messageTopic,t)
  }
  
  dbGetQuery(con,"SET NAMES utf8;")
  products <- dbGetQuery(con, 'SELECT productName, externalId FROM Product;')
  
  if (isChinese) {
    # get message topic labels in Chinese
    messageTopic[, text := paste0(name, documentDescription, emailSubject, sep=" ")]
    
    # generate Chinese label on email topics
    messageTopic <- get_message_topic_labels_cn(messageTopic, products)
    
  } else if (isJapanese){
    messageTopic[, text := paste0(name, documentDescription, emailSubject, sep=" ")]
    messageTopic$text <- gsub(":","",messageTopic$text)
    messageTopic$text <- gsub("-","",messageTopic$text)
    messageTopic$text <- gsub("_","",messageTopic$text)
    messageTopic$text <- gsub("&"," and ",messageTopic$text)
    messageTopic$text <- gsub("^ *|(?<= ) | *$", "", messageTopic$text, perl = TRUE)
    messageTopic$text <- gsub("\n","",messageTopic$text)
    messageTopic$text <- gsub(".com","",messageTopic$text)
    messageTopic$text <- gsub("nbsp;","",messageTopic$text)
    messageTopic$text <- gsub("[{}()0-9]", "", messageTopic$text, perl = TRUE)
    messageTopic$text <- gsub("[】)(|【]","",messageTopic$text)
    messageTopic$text <- gsub("[：/]","",messageTopic$text)
    messageTopic$text <- gsub("[.）]","",messageTopic$text)
    messageTopic$text <- gsub("customText","",messageTopic$text)
    messageTopic <- get_message_topic_labels_jp(messageTopic, products)


  } else {
    # call python function to get the "emailTopicName"
    # messageTopic$emailTopicName <- paste(messageTopic$emailTopicId, messageTopic$productUID, sep="_")
    messageTopicNames <- generate_message_cluster_labels(messageTopic, products)
    messageTopic <- merge(messageTopic, messageTopicNames, by=c("productUID","emailTopicId"), sort=FALSE, all.x=TRUE)
  }
  
  # save results
  FIELDS <- list(messageUID="varchar(40)",productUID="varchar(40)",name="varchar(255)", documentDescription="varchar(255)",emailSubject="varchar(255)",emailTopicId="int(11)", emailTopicName="varchar(255)")
  
  dbWriteTable(con_l,name="AKT_Message_Topic_Email_Learned", value = as.data.frame(messageTopic), overwrite=T, append=F, row.names=FALSE,field.types=FIELDS)
  
  flog.info('Return from messageClustering')
}
