##########################################################
#
#
# aktana-learning Install Aktana Learning Engines.
#
# description: install needed packages for learning model
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2017-01-01
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################
#
# install Packages needed for messageTiming code
#
##########################################################
args <- commandArgs(TRUE)

##args is now a list of character vectors
## First check to see if arguments are passed.
## Then cycle through each element of the list and evaluate the expressions.
if(length(args)==0){
    print("No arguments supplied.")
    quit(save = "no", status = 1, runLast = FALSE)
}else{
    print("Arguments supplied.")
    for(i in 1:length(args)){
      eval(parse(text=args[[i]]))
      print(args[[i]]);
    }
}

# install packages needed by the Engagement functions
needed <- c("futile.logger","RMySQL","data.table","lattice","reshape2","cluster","randomForest","latticeExtra",
  "glmnet","Hmisc","openxlsx","ks","plyr","doMC","fpc","cluster","RColorBrewer","rpart","rpart.plot","lattice",
  "RcppArmadillo","Rcpp","rgl","jpeg","base64enc","text2vec","uuid","properties","RCurl","jsonlite","h2o","testthat",
   "reticulate", "devtools", "ggplot2", "scales")

options(downLoad.file.method="curl")
inst <- installed.packages()
for(i in needed)
  {
      if(!is.element(i,inst))
      {
        if (i == "h2o") {
          install.packages("h2o", type="source", repos="http://h2o-release.s3.amazonaws.com/h2o/rel-yates/1/R")
        } else {
          install.packages(i,repos="https://cran.rstudio.com/")
        }
          
      }
}

# install specific version of data.table
library(devtools)
if (packageVersion("data.table") != "1.11.4") {
  install_version("data.table", version = "1.11.4", repos = "http://cran.us.r-project.org")
}

if (packageVersion("h2o") != "3.24.0.1") {
  remove.packages("h2o")
  install.packages("h2o", type="source", repos="http://h2o-release.s3.amazonaws.com/h2o/rel-yates/1/R")
}

if (packageVersion("reticulate") != "1.10") {
  install_version("reticulate", version = "1.10", repos = "http://cran.rstudio.com")
}

#
# This code builds the learning package and then installs it
#

# R CMD build learningPackage
shellCode <- sprintf("tar -cvf Learning.tar.gz %s/learningPackage",homedir)
system(shellCode)

if(is.element("Learning",inst))remove.packages("Learning")
# R CMD INSTALL Learning_1.0.tar.gz 
install.packages("Learning.tar.gz", repos = NULL, type = "source")

dir.create(sprintf("%s/%s/Runs",homedir,datadir))
dir.create(sprintf("%s/%s/Configurations",homedir,datadir))

