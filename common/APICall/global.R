library(jsonlite)
library(futile.logger)
library(httr)

#**********************************************************************
# Name:     init
# Desc:     Initiate the Serve/DB global variables and setting the values 
# Params:   flag: a TRUE or FALSE flag indicating if its first time, 
#           envdir: directory path containing the env folder
# Returns:  None
#**********************************************************************
init <- function(flag,env){
  if (flag) {
    # env <- fromJSON(sprintf("%s/envs/env.json",envdir))
    
    glblSecret <<- env$secret
    glblLearningAPIurl <<- env$learningApi
    glblUsername <<- env$username
    glblPassword <<- env$password
    # glblBuildVersion <<- env$version
    # glblRefreshInterval <<- env$refreshInterval
    
    # #Enabled Models (Ex: REM;MSM)
    # modules <- strsplit(env$modulesenabled, split = ";")[[1]]
    # glblMSMModueleEnabled <<- extractValue(modules,"MSO")
    # glblREMModueleEnabled <<- extractValue(modules,glblREM)
    
    # glblEnabledProducts <<- env$productsEnabled
    # glblEnabledChannels <<- env$channelsEnabled
    # #glblModelsDir <<- env$modelsDir
    # #glblModelsXls <<- env$modelsXls
    
    glblLoaded <<- flag
  }
}


#**********************************************************************
# Name:     Authenticate
# Desc:     Authenticate the session using the API call to receive a token
# Params:   none
# Returns:  None
#**********************************************************************
Authenticate <- function(url){
  req <- httr::POST(paste(url,"Token/internal",sep = ""),httr::add_headers("Content-Type" = "application/json;charset=UTF-8"),body = paste('{"secret": "', glblSecret, '"}', sep = ""))
  glblToken <<- httr::content(req, as = "text")
  #print(glblToken)
  flog.info("get tokend successfully")
  return(TRUE)
}

#**********************************************************************
# Name:     getAPIJson
# Desc:     Retrives back the api Prints out results to output RS 
# Params:   input, output, session
# Returns:  None
#**********************************************************************
getAPIJson <- function(url,api){
  req <- httr::GET(paste(url, api ,sep = ""), httr::add_headers("Content-Type" = "application/json;charset=UTF-8","Authorization" = paste("Bearer",glblToken)))
  #json <- httr::content(req, as ="text")
  #ParameterControlList <- fromJSON(json)
  if (httr::status_code(req) == 200)
  {
    flog.info("GET::%s successful", paste(url, api ,sep = ""))
    return(req)
  }
  else 
  {
    flog.error("GET::%s failed:%s",paste(url, api ,sep = ""), paste(httr::content(req)$code,httr::content(req)$message,sep = " "))
    stop(sprintf("GET::%s failed", paste(url, api ,sep = "")))
  }
}

postAPIJson <- function(url, api,params){ 
  req <- httr::POST(paste(url, api ,sep = ""), httr::add_headers("Content-Type" = "application/json;charset=UTF-8","Authorization" = paste("Bearer",glblToken)), body = params)
  if (httr::status_code(req) == 200)
  {
    flog.info("POST::%s successful", paste(url, api ,sep = ""))
    return(req)
  }
  else 
  {
    flog.error("POST::%s failed:%s",paste(url, api ,sep = ""), paste(httr::content(req)$code,httr::content(req)$message,sep = " "))
    stop(sprintf("POST::%s failed", paste(url, api ,sep = "")))
  }    
}

deleteAPIJson <- function(url, api){ 
  req <- httr::DELETE(paste(url, api ,sep = ""), httr::add_headers("Content-Type" = "application/json;charset=UTF-8","Authorization" = paste("Bearer",glblToken)))
  #json <- httr::content(req, as ="text")
  #ParameterControlList <- fromJSON(json)
  if (httr::status_code(req) == 200)
  {
    flog.info("DELETE::%s successful", paste(url, api ,sep = ""))
    return(req)
  }
  else
  {
    flog.error("DELETE::%s failed:%s",paste(url, api ,sep = ""), paste(httr::content(req)$code,httr::content(req)$message,sep = " "))
    stop(sprintf("DELETE::%s failed", paste(url, api ,sep = "")))
  }    
}

putAPIJsonRaw <- function(url, api,params=NULL){
  if (is.null(params)) {
    flog.info("no params put into PUT call")
    req <- httr::PUT(paste(url, api ,sep = ""), httr::add_headers("Content-Type" = "text/plain","Authorization" = paste("Bearer",glblToken)),body = params)
  } else {
    req <- httr::PUT(paste(url, api ,sep = ""), httr::add_headers("Content-Type" = "application/json;charset=UTF-8","Authorization" = paste("Bearer",glblToken)), body = params)
  }
  
  return(req)
}

putAPIJson <- function(url, api,params=NULL){
  req <- putAPIJsonRaw(url, api,params)
  
  if (httr::status_code(req) == 200)
  {
    flog.info("PUT::%s successful", paste(url, api ,sep = ""))
    return(req)
  }
  else
  {
    flog.error("PUT::%s failed:%s",paste(url, api ,sep = ""), paste(httr::content(req)$code,httr::content(req)$message,sep = " "))
    stop(sprintf("PUT::%s failed", paste(url, api ,sep = "")))
  }
}

#**********************************************************************
# Name:     GenerateDictlistFromAPIRes
# Desc:     Genreates a dictionary-like list object of the with values as elementes, and key as names
# Params:   apiRes: an api response to extract values from
#           key: a key column to extract key vaule
#           value: a value column to extract values
# Returns:  a list object of the with values as elementes, and key as names
#**********************************************************************
GenerateDictlistFromAPIRes <- function(apiRes,key,value) {
  lst <- lapply(apiRes, function(x){return(as.character(x[[value]]))})
  listNames <- sapply(apiRes, function(x){return(as.character(x[[key]]))})
  names(lst) <- listNames
  return(lst)
}

#**********************************************************************
# Name:     getAccountAccountProductIdNameMap
# Desc:     
# Params:   
# Returns:  a list object of the with predictor names to be displayed 
#           on UI as elementes, and predictor column names in Account/
#           AccountProdcut table in names
#**********************************************************************
getAccountAccountProductIdNameMap <- function() {
  res <- httr::content(getAPIJson(glblLearningAPIurl,"DSE/referencedata"))
  accountMap <- GenerateDictlistFromAPIRes(res$referenceData$accountAttributes,'id','name')
  accountProductMap <- GenerateDictlistFromAPIRes(res$referenceData$metrics,'id','name')
  return(c(accountMap,accountProductMap))
}
