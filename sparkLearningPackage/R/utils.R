#' Given name, return corresponding SQL function
sqlf <- function(sc, f) {
  return (function(x, ...) {invoke_static(sc, "org.apache.spark.sql.functions", f, x, ...)})
}

jdfFuncWrapper <- function(func,df,...) {
  # Alias for the output view
  alias <- paste(deparse(substitute(df)), stri_rand_strings(1, 10), sep = "_")
  # Get session and JVM object
  sc <- spark_connection(df)
  # spark <- spark_session(sc)
  jdf <- spark_dataframe(df)
  # run func
  jdf <- func(sc,jdf,...)
  # send to view
  jdf %>% invoke("createOrReplaceTempView", alias)
  # return
  return(dplyr::tbl(sc, alias))
}