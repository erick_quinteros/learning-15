

getOptimalParametersFromDb <- function(productUID, channelUID, goal, con_l)
{
  # Prepare SQL Query
  sql_query <- sprintf("SELECT paramName, paramValue FROM OptimalLearningParams WHERE productUID='%s' AND channelUID='%s' AND goal='%s'", productUID, channelUID, goal)
  optimalParams <- dbGetQuery(con_l, sql_query)
  
  return(optimalParams)
}

updateLearningConfigForOptimalParameters <- function(config, con_l)
{
  # Read the ProductUID, ChannelUID, Goal
  productUID <- getConfigurationValueNew(config, "productUID")
  channelUID <- getConfigurationValueNew(config, "channelUID")
  goal <- getConfigurationValueNew(config, "LE_MS_messageAnalysisTargetType")
  
  # Read the optimal parameters from the OptimalLearningParams table in learning schema
  optimalParams <- getOptimalParametersFromDb(productUID, channelUID, goal, con_l)
  
  # Set the parameters to the config
  config[["LE_MS_addPredictorsFromAccountProduct"]] <- optimalParams$paramValue[optimalParams$paramName == "LE_MS_addPredictorsFromAccountProduct"]
  config[["LE_MS_includeVisitChannel"]] <- optimalParams$paramValue[optimalParams$paramName == "LE_MS_includeVisitChannel"]
  config[["LE_MS_removeMessageClicks"]] <- optimalParams$paramValue[optimalParams$paramName == "LE_MS_removeMessageClicks"]
  config[["LE_MS_removeMessageOpens"]] <- optimalParams$paramValue[optimalParams$paramName == "LE_MS_removeMessageOpens"]
  config[["LE_MS_removeMessageSends"]] <- optimalParams$paramValue[optimalParams$paramName == "LE_MS_removeMessageSends"]
  
  return(config)
}